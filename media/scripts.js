
var Base = {

    init: function() {
        $("#buscar").click(function() {
            Base.buscar();
        });
    },

    buscar: function() {
        $(".resultados").html("Aguarde...");
        $(".header").hide();

        $.ajax({
            url: '/buscar/',
            type: 'POST',
            dataType: 'json',
            data:{
                'localidade': $("#localidade").val(),
                'atividade': $('#atividade').val()
            },
            error: function(){
                alert("erro ao pesquisar");
                $(".resultados").empty();
                $(".header").show();
            },
            success: function(response){
                $(".resultados").empty();
                $.each(response, function(idx, value) {
                    var elem = $("#resultado").clone();
                    elem.find('.nome').html(value.nome);
                    elem.find('.telefone').html(value.telefones[0]);
                    elem.find('.endereco_1').html(value.endereco_1);
                    elem.find('.endereco_2').html(value.endereco_2);
                    elem.show().appendTo(".resultados");

                });
                $(".header").show();
            }
        });
    }

};

$(document).ready(function() {
    Base.init();
});
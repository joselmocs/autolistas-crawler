# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse


def base(request):
    from lib.telelista import Telelista

    context = RequestContext(request)
    c = {
        'localidades': Telelista.localidades,
        'atividades': Telelista.atividades
    }

    return render_to_response('base.html', c, context_instance=context)


def buscar(request):
    from lib.telelista import Telelista
    from django.utils.simplejson import dumps

    Telelista.instance().iniciar()
    resultado = Telelista.instance().crawler.buscar(
        atividade=request.POST.get('atividade'),
        localidade=request.POST.get('localidade').split('-')[0],
        uf=request.POST.get('localidade').split('-')[1]
    )

    return HttpResponse(dumps(resultado), mimetype="application/json")

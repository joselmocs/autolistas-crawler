# -*- coding: utf-8 -*-
import os
import math
from PIL import Image
from django.conf import settings


class Captcha:

    list_color = [8]
    iconset = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
    guess = []

    def __init__(self, image=None):
        im2 = Image.new("P", image.size, 255)
        im = image.convert("P")
        temp = {}

        v = VectorCompare()
        inletter = False
        foundletter = False
        letters = []
        start = 0

        for x in range(im.size[1]):
            for y in range(im.size[0]):
                pix = im.getpixel((y, x))
                temp[pix] = pix
                if pix in self.list_color:
                    im2.putpixel((y, x), 0)

        for y in range(im2.size[0]):
            for x in range(im2.size[1]):
                pix = im2.getpixel((y, x))
                if pix != 255:
                    inletter = True

            if not foundletter and inletter:
                foundletter = True
                start = y

            if foundletter and not inletter:
                foundletter = False
                end = y
                letters.append((start, end))

            inletter = False

        count = 0

        self.guess = []
        for letter in letters:
            im3 = im2.crop((letter[0], 0, letter[1], im2.size[1]))

            guess = []
            for image in self.imageset():
                for x, y in image.iteritems():
                    if len(y) != 0:
                        guess.append((v.relation(y[0], self.buildvector(im3)), x))

            guess.sort(reverse=True)
            self.guess.append(guess[0][1])
            count += 1

    def buildvector(self, im):
        d1 = {}
        count = 0
        for i in im.getdata():
            d1[count] = i
            count += 1
        return d1

    def imageset(self):
        imageset = []
        iconset_dir = os.path.join(settings.PROJECT_ROOT_PATH, 'lib')

        for letter in self.iconset:
            for img in os.listdir(iconset_dir + '/iconset/%s/' % letter):
                temp = []
                if img != "Thumbs.db":
                    temp.append(self.buildvector(Image.open(iconset_dir + "/iconset/%s/%s" % (letter, img))))

                imageset.append({letter: temp})

        return imageset

    def crack(self):
        return ''.join(self.guess)


class VectorCompare:

    def magnitude(self, concordance):
        total = 0
        for word, count in concordance.iteritems():
            total += count ** 2
        return math.sqrt(total)

    def relation(self, concordance1, concordance2):
        topvalue = 0
        for word, count in concordance1.iteritems():
            if concordance2.has_key(word):
                topvalue += count * concordance2[word]
        return topvalue / (self.magnitude(concordance1) * self.magnitude(concordance2))
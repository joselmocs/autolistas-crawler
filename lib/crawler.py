# -*- coding: utf-8 -*-

import requests
from log import Logger


class Crawler:

    request = None
    response = None
    logger = "crawler"

    class Url:
        PESQUISA = "templates/resultado_busca.aspx"

    def __init__(self):
        """
        Abre o site inicialmente para guardar a sessão e os cookies
        """
        from telelista import Telelista

        Logger.info(self, "Abrindo sessao no site...")

        self.reiniciar_sessao()
        self.response = self.request.get("%s" % Telelista.host)

        if self.response.status_code == 200:
            Logger.info(self, "Sessao aberta com sucesso.")
        else:
            Logger.error(self, "Nao foi possivel conectar ao site. Status [%s]." % self.response.status_code)

    def reiniciar_sessao(self):
        """
        Re-Inicia uma instancia da Sessão
        """
        self.request = requests.Session()

    def buscar(self, atividade, localidade, uf):
        """
        Busca a paginação das empresas de acordo com os parametros enviados
        """
        from PIL import Image
        from captcha import Captcha
        from bs4 import BeautifulSoup
        from StringIO import StringIO
        from telelista import Telelista

        self.response = self.request.get(
            "%s/%s?%s" % (Telelista.host, self.Url.PESQUISA,
                          "cod_localidade=%s&atividade=%s&uf_busca=%s" % (localidade, atividade, uf))
        )

        if not self.response.status_code == 200:
            Logger.error(self, "Nao foi possivel conectar ao site. Status [%s]." % self.response.status_code)

        conteudo = BeautifulSoup(self.response.content, "html5lib")

        resultados = conteudo.find_all(
            name='table', attrs={'bgcolor': '#f0f0f0'}
        )

        empresas = []
        for resultado in resultados:
            frag_a = resultado.find_all(name='td', attrs={'class': 'text_resultado_ib'})
            frag_b = resultado.find_all(name='td', attrs={'class': 'text_endereco_ib'})[1].text.split('\n')

            telefone = frag_a[1].text.split(':')[1].split(' mais')[0].strip()

            imagem = self.request.get(frag_a[1].find(name='img').__str__().split('"')[1].replace('amp;', ''))

            captcha = Captcha(Image.open(StringIO(imagem.content))).crack()

            empresas.append({
                'nome': frag_a[0].text.strip(),
                'telefones': ["%s%s" % (telefone, captcha)],
                'endereco_1': frag_b[0].strip(),
                'endereco_2': frag_b[1].strip()
            })

        return empresas

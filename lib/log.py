# -*- coding: utf-8 -*-
from datetime import datetime


class Logger:

    @staticmethod
    def getTime():
        return datetime.now().strftime("%H:%M:%S")

    @staticmethod
    def write(message):
        print message

    @staticmethod
    def info(self, message):
        Logger.write('[%s] [info] [%s] %s' % (Logger.getTime(), self.logger, message))

    @staticmethod
    def error(self, message):
        Logger.write('[%s] [erro] [%s] %s' % (Logger.getTime(), self.logger, message))

        raise RuntimeError()

# -*- coding: utf-8 -*-


class Telelista:

    host = "http://www.telelistas.net"
    crawler = None
    class_instance = None

    atividades = ['Chaveiros', 'Chaveiros 24h', 'Borracheiros', 'Pneus', 'Betoneiras', 'Empilhadeiras']
    localidades = [
        {'id': 41000, 'cidade': "Curitiba", 'uf': "pr"},
        {'id': 31000, 'cidade': "Belo Horizonte", 'uf': "mg"}
    ]

    class Url:
        pass

    @staticmethod
    def instance():
        """
        Retorna uma unica instancia do inicializador
        """
        if not Telelista.class_instance:
            Telelista.class_instance = Telelista()

        return Telelista.class_instance

    def iniciar(self):
        from crawler import Crawler

        self.crawler = Crawler()

#Telelista.instance().iniciar()
#Telelista.instance().crawler.buscar(atividade=Telelista.atividades[2], localidade="31000", uf="mg")

#exit(0)